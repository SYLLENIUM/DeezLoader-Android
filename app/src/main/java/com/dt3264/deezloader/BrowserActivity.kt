package com.dt3264.deezloader

import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity
import androidx.webkit.WebViewClientCompat

class BrowserActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browser)

        findViewById<WebView>(R.id.internalWebView).apply {
            webViewClient = WebViewClientCompat()
            settings.apply {
                domStorageEnabled = true
                javaScriptEnabled = true
                databaseEnabled = true
                allowFileAccess = true
                allowFileAccessFromFileURLs = true
                allowContentAccess = true
            }
        }.loadUrl(serverURL)
    }
}
